# Ornament service

Ornament PDF OCR service based on [Django framework](https://www.djangoproject.com/).

## Summary

Source repositories:
- Backend — https://bitbucket.org/sakkada/ornament-ocr-service/

Servers:
- Development — http://factory.sakkada.ru, admin — http://factory.sakkada.ru/admin/ (root/rootpass)

## Docker

### Installation

To build web site in docker, please build images and run them via compose:
```
$ docker-compose build
$ docker-compose up
```

After all services will be started, run the following commands
to prepare db and create super user:
```
$ docker-compose run web python code/project/manage.py migrate
$ docker-compose run web python code/project/manage.py createsuperuser
```

### Run

To run project in docker, please enter the following command:
```
$ docker-compose up
```

To connect to running contaner, please enter:
```
$ docker-compose exec web /bin/bash
```

You can use any linux shell utility or program instead of `/bin/bash`,
e.g. `ls`, `python`, `sh` or `apt` etc.
