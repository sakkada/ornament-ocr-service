FROM python:3.6
ENV PYTHONUNBUFFERED 1

# generating directories structure
RUN mkdir /django/build/requirements -p && \
    mkdir /django/build/src -p && \
    mkdir /django/code/datadir/temporary -p && \
    mkdir /django/code/datadir/logs -p

WORKDIR /django

ADD requirements/base.txt /django/build/requirements
ADD requirements/development.txt /django/build/requirements
ADD requirements/production.txt /django/build/requirements

RUN pip install pip --upgrade --trusted-host files.pythonhosted.org
RUN pip install --src build/src -r build/requirements/development.txt
