from project.settings.base import *

DEBUG = True
TEMPLATES[0]['OPTIONS']['debug'] = DEBUG

ALLOWED_HOSTS = ('127.0.0.1', '.sakkada.ru', '.local',)

# SENTRY_CONFIG['init']['environment'] = 'development'
