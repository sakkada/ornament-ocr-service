from django.urls import path
from . import views


app_name = 'main'
urlpatterns = [
    path(r'', views.index, name='index'),
]
