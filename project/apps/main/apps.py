from django.apps import AppConfig
from django.contrib.admin.apps import AdminConfig
from django.utils.translation import ugettext_lazy as _


class MainConfig(AppConfig):
    name = 'main'
    verbose_name = _('Main Application')

    def ready(self):
        # initialize sentry_sdk
        # from .utils import sentry_sdk_init
        # sentry_sdk_init()
        pass


class AdminConfig(AdminConfig):
    pass
