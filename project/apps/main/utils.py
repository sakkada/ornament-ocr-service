import subprocess
import sentry_sdk
import sentry_sdk.integrations.django
from django.conf import settings
from django.contrib.sites.models import Site


def get_site_url(site=None, scheme=None):
    site = site or Site.objects.get_current()
    scheme = scheme or settings.SITE_SCHEME
    return '%s://%s' % (settings.SITE_SCHEME, Site.objects.get_current(),)


def get_vcs_hash(vcs=None, default=None):
    vcsmap = {
        'hg': ('hg', 'id', '-i', '--debug'),
        'git': ('git', 'rev-parse', 'HEAD'),
    }
    vcs = vcsmap.get(vcs, None)

    if not vcs:
        return default
    try:
        hash = subprocess.check_output(
            vcs, shell=False, cwd=settings.BASE_DIR).strip().decode('utf8')
    except subprocess.CalledProcessError as e:
        hash = default
    return hash


def sentry_sdk_init():
    if not settings.SENTRY_CONFIG.get('enabled', False):
        return

    kwargs = settings.SENTRY_CONFIG['init']
    kwargs['integrations'] = [
        sentry_sdk.integrations.django.DjangoIntegration()
    ]

    vcs = settings.SENTRY_CONFIG.get('release_vcs_hash', None)
    vcs = vcs and get_vcs_hash(vcs)
    if vcs:
        kwargs['release'] = vcs

    sentry_sdk.init(**kwargs)
