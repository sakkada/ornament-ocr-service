from django.conf import settings
from .storage.hashed import (
    HashedNameFileSystemStorage as HNFSS
)


hashed_storage = HNFSS(location=settings.MEDIA_ROOT,
                       uniquify_names=True, segments=(1,2,))
