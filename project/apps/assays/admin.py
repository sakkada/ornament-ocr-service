import json
import logging
import datetime
from django import forms
from django.contrib import admin
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _
from ocr import PDFProcessor, InvirtoAssayParser
from . import models


logger = logging.getLogger(__name__)


# Admin forms
# -----------
class AssayForm(forms.ModelForm):
    process_file = forms.BooleanField(required=False)

    class Meta:
        model = models.Assay
        fields = '__all__'


# Admin classes
# -------------
class AssayValueInlineAdmin(admin.TabularInline):
    model = models.AssayValue
    extra = 0


class AssayAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'client', 'eid',)
    form = AssayForm
    fieldsets = [
        (None, {'fields': list(AssayForm.base_fields),},),
        ('Data (json)', {'classes': ('collapse',), 'fields': ('get_data',),},),
    ]
    readonly_fields = ('get_data',)
    inlines = (AssayValueInlineAdmin,)

    def get_data(self, obj):
        return mark_safe('<pre>%s</pre>' % obj.data)
    get_data.short_description = 'Data (JSON)'

    def save_model(self, request, obj, form, change):
        obj.save()
        if form.cleaned_data['process_file']:
            if obj.status == 'success':
                logger.debug('Assys already processed.')
                return

            data = PDFProcessor().process(obj.file.path)
            if not data['errors']:
                obj.client = data['data']['client']
                obj.title = data['data']['title']
                obj.sex = data['data']['meta']['Возраст']
                obj.age = data['data']['meta']['ИНЗ']
                obj.eid = data['data']['meta']['Пол']

                obj.date_take =datetime.datetime.strptime(
                    data['data']['meta']['Дата взятия образца'], '%d.%m.%Y %H:%M')
                obj.date_receive = datetime.datetime.strptime(
                    data['data']['meta']['Дата поступления образца'], '%d.%m.%Y %H:%M')
                obj.date_print = datetime.datetime.strptime(
                    data['data']['meta']['Дата печати результата'], '%d.%m.%Y %H:%M')

                obj.status = 'success'

                obj.assayvalue_set.all().delete()
                for i in data['data']['assays']:
                    models.AssayValue.objects.create(
                        assay=obj,
                        title=i['Исследование'],
                        value=i['Результат'],
                        unit=i['Единицы'],
                        range=i['Референсные значения'],
                        min=i['minmax'][0],
                        max=i['minmax'][1],
                    )

                logger.debug('Assys successfully processed.\n%s' % obj.data)
            else:
                obj.status = 'failure'

            obj.data = json.dumps(data, indent=2, ensure_ascii=False)
            obj.save()

admin.site.register(models.Assay, AssayAdmin)
admin.site.register(models.AssayValue)
