from django.db import models
from django.utils.translation import ugettext_lazy as _
from main.files import hashed_storage
from main import validators


FILES_UPLOAD_TO = {
    'assay__file': 'assays/assay/file/',
}


class Assay(models.Model):
    STATUS_CHOICES = (
        ('initial', _('initial'),),
        ('success', _('success'),),
        ('failure', _('failure'),),
    )

    status = models.CharField(
        _('status'), max_length=32,
        choices=STATUS_CHOICES, default=STATUS_CHOICES[0][0])
    file = models.FileField(
        _('pdf file'), max_length=1024, storage=hashed_storage,
        upload_to=FILES_UPLOAD_TO['assay__file'],
        validators=[
            validators.ExtensionValidator(('.pdf',)),
            validators.FilesizeValidator(max=1024*1024*5),
        ]
    )
    data = models.TextField(
        _('data (json)'), max_length=1024*32, blank=True, editable=False)

    eid = models.CharField(_('eid'), max_length=32, blank=True)
    title = models.CharField(_('title'), max_length=1024, blank=True)
    client = models.CharField(_('client'), max_length=1024, blank=True)
    age = models.CharField(_('age'), max_length=32, blank=True)
    sex = models.CharField(_('sex'), max_length=32, blank=True)

    # dates
    date_take = models.DateTimeField(
        _('date take assay'), blank=True, null=True)
    date_receive = models.DateTimeField(
        _('date receive assay'), blank=True, null=True)
    date_print = models.DateTimeField(
        _('date print assay'), blank=True, null=True)

    # stat info
    date_create = models.DateTimeField(editable=False, auto_now_add=True)
    date_update = models.DateTimeField(editable=False, auto_now=True)

    class Meta:
        verbose_name = _('Assay')
        verbose_name_plural = _('Assays')
        ordering = ('-date_create', '-id')

    def __str__(self):
        return '<Assay #%s>' % self.id


class AssayValue(models.Model):
    assay = models.ForeignKey(
        Assay, verbose_name=_('assay'), on_delete=models.CASCADE)

    title = models.CharField(_('title'), max_length=64)
    value = models.CharField(_('result'), max_length=64)
    unit = models.CharField(_('unit'), max_length=64)
    range = models.CharField(_('range'), max_length=64)

    min = models.FloatField(_('min'), null=True, blank=True)
    max = models.FloatField(_('max'), null=True, blank=True)

    # stat info
    date_create = models.DateTimeField(editable=False, auto_now_add=True)
    date_update = models.DateTimeField(editable=False, auto_now=True)

    class Meta:
        verbose_name = _('Assay value')
        verbose_name_plural = _('Assay values')
        ordering = ('-date_create', '-id')

    def __str__(self):
        return '<Assay #%s value #%s>' % (self.assay_id, self.id,)
