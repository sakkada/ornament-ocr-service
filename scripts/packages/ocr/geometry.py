def sort_rects_by_topleft(a, b):
    ra, rb = Rectangle(*a.bbox), Rectangle(*b.bbox)
    if ra.has_equal_position(rb, y2=3):
        return -1 if ra.x1 <= rb.x1 else 1
    else:
        return -1 if ra.y2 >= rb.y2 else 1


class Rectangle:
    def __init__(self, x1, y1, x2, y2):
        self.x1 = x1
        self.y1 = y1
        self.x2 = x2
        self.y2 = y2

    def is_intersect(self, other):
        if self.x1 > other.x2 or self.x2 < other.x1:
            return False
        if self.y1 > other.y2 or self.y2 < other.y1:
            return False
        return True

    def is_cover(self, other):
        return (self.x1 <= other.x1 and self.x2 >= other.x2 and
                self.y1 <= other.y1 and self.y2 >= other.y1)

    def is_covered(self, other):
        return other.is_cover(self)

    def get_insersection(self, other):
        if not self.is_intersect(other):
            return None
        return Rectangle(max(self.x1, other.x1), max(self.y1, other.y1),
                         min(self.x2, other.x2), min(self.y2, other.y2))

    def get_insersection_percent(self, other):
        percent = 0
        if self.is_intersect(other):
            percent = self.get_insersection(other).get_area() / other.get_area()
        return percent if percent < 100 else 100

    def get_area(self):
        return (self.x2 - self.x1) * (self.y2 - self.y1)

    def has_equal_position(self, other,
                           x1=None, y1=None, x2=None, y2=None, accuracy=0):
        values = ((x1, 'x1',), (y1, 'y1',), (x2, 'x2',), (y2, 'y2',),)
        result = {'x1': None, 'y1': None, 'x2': None, 'y2': None,}
        for value, name in values:
            if value is None:
                result[name] = True
                continue
            distance = value if isinstance(value, (int, float,)) else accuracy
            result[name] = (getattr(self, name) - distance <=
                            getattr(other, name) <=
                            getattr(self, name) + distance)
        return all(result.values())

    def __str__(self):
        return 'Rectangle' + str((self.x1, self.y1, self.x2, self.y2))


if __name__ == "__main__":
    r1 = Rectangle(0, 0, 10, 10)
    r2 = Rectangle(5, 5, 15, 15)
    r3 = Rectangle(0, 0, 12, 13)
    print(r1.is_intersect(r2))
    print(r1.get_insersection(r2))
    print(r1.has_equal_position(r3, x2=3))
    print(r1.get_area())
