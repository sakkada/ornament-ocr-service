import os
import re
import sys
import json

from binascii import b2a_hex
from functools import cmp_to_key, reduce

from pdfminer.pdfparser import PDFParser
from pdfminer.pdfdocument import PDFDocument
from pdfminer.pdfpage import PDFPage
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import PDFPageAggregator
from pdfminer.layout import LAParams, LTTextBox

from .geometry import Rectangle, sort_rects_by_topleft


class PDFProcessor:
    def process(self, filename, password=None):
        errors, data = {}, None
        try:
            fp = open(filename, 'rb')
            parser = PDFParser(fp)
            document = PDFDocument(parser, password=password or '')
            if not document.is_extractable:
                errors['common'] = 'Document is not extractable.'
            else:
                rsrcmgr = PDFResourceManager()
                laparams = LAParams()
                device = PDFPageAggregator(rsrcmgr, laparams=laparams)
                interpreter = PDFPageInterpreter(rsrcmgr, device)
                pages = []
                for page in PDFPage.create_pages(document):
                    interpreter.process_page(page)
                    layout = device.get_result()
                    pages.append({'page': page, 'layout': layout,})

                for key, AssysClass in ASSAYS.items():
                    handler = AssysClass()
                    if not handler.check(document, pages):
                        continue
                    data, errors = handler.generate(document, pages), handler.errors
                    if data and not errors:
                        break

                if not data and not errors:
                    errors['common'] = 'No any parser detects correct format.'

        except IOError as e:
            errors['common'] = 'Document is not exists or not accessible.'
        # except Exception as e:
        #     data, errors['common'] = None, str(e)
        else:
            fp and fp.close()

        return {'data': data, 'errors': errors or None,}


class BaseAssayParser:
    errors = None

    def check(self, document):
        # Method returns bool
        raise NotImplementedError

    def generate(self, document):
        # Method returns data dict or None
        raise NotImplementedError


class InvirtoAssayParser(BaseAssayParser):
    def check(self, document, pages):
        # Early validation section
        # ------------------------
        KEYLINE = 'ООО "ИНВИТРО"'

        page, layout = pages[0]['page'], pages[0]['layout']
        mediabox = page.mediabox
        textboxes = [i for i in pages[0]['layout'] if isinstance(i, LTTextBox)]

        # check page count
        if len(pages) < 2:
            return False

        # check keyline (lab title) located in top right sector
        cover_rect = Rectangle(mediabox[2] * 0.4, mediabox[3] * 0.8,
                               mediabox[2], mediabox[3])

        lines = reduce(list.__add__, [list(i) for i in textboxes[:5]])
        lines = list(filter(lambda x: cover_rect.is_cover(Rectangle(*x.bbox)), lines))

        return any(KEYLINE in i.get_text().strip() for i in lines)

    def generate(self, document, pages):
        KEYLINE = 'Исследование'
        self.errors = {}

        # Validation section
        # ------------------

        page, layout = pages[0]['page'], pages[0]['layout']
        mediabox = page.mediabox

        # check page count
        if len(pages) < 2:
            self.errors['page_count_too_small'] = 'Page count less than 2.'
            return None

        # check first LT elements (textboxes)
        textboxes = [i for i in pages[0]['layout'] if isinstance(i, LTTextBox)]
        if not len(textboxes) >= 6:
            self.errors['textbox_count_too_small'] = (
                'Layout should contains at least six TextBoxes.')
            return None

        keytextbox = list(filter(
            lambda x: any((i.get_text().strip() == KEYLINE for i in x[1])),
            enumerate(textboxes)))
        keytextbox_index, keytextbox = keytextbox and keytextbox[0] or (None, 0,)
        keytextbox_lines = keytextbox and list(keytextbox)

        if not keytextbox:
            self.errors['key_textbox_not_found'] = (
                'Key TextBox which should contains "{KEYLINE}" if not found.')
            return None

        keyline_index, keyline = list(filter(
            lambda x: x[1].get_text().strip() == KEYLINE,
            enumerate(keytextbox)))[0]

        # check meta data location and structure
        meta_cover_rect = Rectangle(keyline.bbox[0] - 10, keyline.bbox[1] + 5,
                                    mediabox[2] * 0.7, mediabox[3])

        meta_lines = reduce(list.__add__, [list(i) for i in textboxes[:keytextbox_index+1]])
        meta_lines = list(filter(lambda x: meta_cover_rect.is_cover(Rectangle(*x.bbox)), meta_lines))
        meta_lines = sorted(meta_lines, key=cmp_to_key(sort_rects_by_topleft))
        if not all(i.get_text().strip().endswith(':') for i in meta_lines[1:-1:2]):
            self.errors['meta_lines_structure_error'] = (
                'Meta data lines are incorrect (not all have : at the end).')
            return None

        # check main textbox place and size
        keytextbox_place = Rectangle(*mediabox[:2], mediabox[2] * 0.6, mediabox[3])
        if not keytextbox_place.is_cover(Rectangle(*keytextbox.bbox)):
            self.errors['key_textbox_geometry_too_large'] = 'Key TextBox is too large.'
            return None

        # check main textbox content
        # - keytextbox should contains at least (
        #       annots count +
        #       keyline_index (keyline and all before it) +
        #       one last line with asterisk
        #   ) lines
        # - keytextbox should contains * ... at last position.

        if not len(keytextbox_lines) <= len(page.annots) + keyline_index + 2:
            self.errors['key_textbox_lines_count_too_small'] = (
                'Key TextBox lines count is too small.')
            return None
        if not keytextbox_lines[-1].get_text().strip().startswith('* '):
            self.errors['key_textbox_asterisk_line_not_found'] = (
                'Key TextBox does not contain "* ..." at last position.')
            return None

        # check correctly located annots count (should be equal to column lines)
        annots_list = [i.resolve() for i in page.annots]
        annots_list = [(Rectangle(*i['Rect']), i) for i in annots_list]
        annots = []
        for index, line in enumerate(keytextbox_lines[keyline_index+1:-1]):
            line_rect = Rectangle(*line.bbox)
            annot_rect, annot = annots_list[index]

            if line_rect.has_equal_position(annot_rect, x1=3, x2=3, y1=3, y2=3):
                annot_valid = annot
            else:
                annot_valid = None
                for annot_rect, annot in annots:
                    if line_rect.has_equal_position(annot_rect, x1=3, x2=3, y1=3, y2=3):
                        annot_valid = annot
                        break
            if annot_valid:
                annots.append(annot_valid)
                continue

            self.errors['first_column_line_related_annot_not_found'] = (
                'Not all first column lines have realted annotation (links).')
            return None

        # KEYLINE should has the same height (y2) as 3+ rest columns
        keytextbox_column_title = Rectangle(*keyline.bbox)
        columns = [i for i in textboxes if
                   Rectangle(*i.bbox).has_equal_position(keytextbox_column_title, y2=3)]

        if not len(columns) >= 3:
            self.errors['columns_count_too_samll'] = 'TextBox columns count too small (<4).'
            return None

        # Columns lines count equality
        keytextbox_column_lines = len(keytextbox_lines) - keyline_index - 1
        if not all(len(list(i)) == keytextbox_column_lines for i in columns):
            self.errors['columns_lines_count_not_equal'] = (
                'TextBox columns lines count should be equal (table structure error).')
            return None

        # Generation section
        # ------------------

        # get meta information
        meta = meta_lines[1:-1]
        meta = [meta[i:i+2] for i in range(0, len(meta), 2)]
        meta = {k.get_text().strip(': \n'): v.get_text().strip() for k, v in meta}

        # get assays information
        assays = [list(map(lambda x: x.get_text().strip(), i))
                  for i in zip(keytextbox_lines[keyline_index:-1], *columns)]
        assays = list(map(lambda x: dict(zip(assays[0], x)), assays[1:]))
        for assay, annot in zip(assays, annots):
            assay['url'] = annot['A']['URI'].decode('utf-8')
            assay['minmax'] = self._parse_value_range(assay['Референсные значения'])

        return {
            'client': meta_lines[0].get_text().strip(),
            'title': meta_lines[-1].get_text().strip(),
            'meta': meta,
            'assays': assays,
        }

    def _parse_value_range(self, value):
        pattern = (r'^\s*<\s*(\d+(?:\.\d+)?)$'
                   r'|^\s*>\s*(\d+(?:\.\d+)?)$'
                   r'|^(\d+(?:\.\d+)?)\s*-\s*(\d+(?:\.\d+)?)$')

        match = re.match(pattern, value)
        if match:
            groups = match.groups()
            if groups[0]:
                return [0, float(groups[0])]
            if groups[1]:
                return [float(groups[1]), 99999]
            else:
                return [float(groups[2]), float(groups[3])]
        else:
            return [None, None]


ASSAYS = {
    'invirto': InvirtoAssayParser,
}


if __name__ == '__main__':
    data = PDFProcessor().process(sys.argv[1])
    print(json.dumps(data, indent=2, ensure_ascii=False))
